# Code for Rivera-Colon et al. (2012)

Repository containing bioinformatic code used for analysis of RAD-seq data.

## Citations

Rivera-Colón et al. (2020) Multiple loci control eyespot number variation on the hindwings of *Bicyclus anynana* butterflies. Genetics. [10.1534/genetics.120.303059](https://doi.org/10.1534/genetics.120.303059)

## Raw Reads

Demultiplexed raw reads for the *Bicyclus anynana* *PstI* RAD-seq library are available at NCBI BioProject (`PRJNA509697`).

## Pipeline

### Clean raw reads

Script: `process_radtags_pstI_80bp.sh`

Use *Stacks* `process_radtags`. Check for paired-end reads (`-P`), remove Ns (`-c`), check quality scores (`-q`), and rescue barcodes (`-r`). Trim reads to 80bp (`-t 80`), Use enzyme *PstI* (`-e pstI`).

### Align reads to reference genome

Script: `bwa_alignment-to_BAM.sh`

Reference genome is `BANY.1.2` from <http://ensembl.lepbase.org/Bicyclus_anynana_v1x2/Info/Index.>

Align reads to genome using `bwa mem` and process to `.bam` files using `samtools view`.

### Process alignments

Script: `process_alignments.sh`

Sort and index reads using `samtools sort` and `samtools index`. Remove duplicate reads and add read groups using `picardtools`.

### Genotype with GATK

Script: `gatk_genotype.sh`

Use `GATK UnifiedGenotyper`

#### Filter genotypes with VCFtools

Script: `vcftools_filter_snps.sh`

Filter genotypes by removing indels, low quality calls, genotypes with >50% missing data, non-biallelic, depth <5X.

### Assemble RAD loci with Stacks

Script: `run_stacks_ref.sh`

Use `ref_map.pl` from *Stacks* to assemble RAD loci

#### Filter assembled loci and genotypes with Stacks

Script: `stacks_populations.sh`

Calculate F-statistics, kernel smooth in 50Kb windows, correct p-values, and filter loci/genotypes not present in both populations (`-p 2`) and in 75% samples per population (`-r 0.75`)

## Genome wide association

Calculate association to dorsal hindwing eyespot number

### Format output to *PLINK* format

Script: `convert-ped-bed.sh`

From filtered VCF, generate *PLINK* output with *Stacks*. Use *PLINK* to add additional formatting and compress.

### GWAS with GEMMA

Script: `gemma_gwas.sh`

Generate relatedness matrix and calculate GWA using a linear mixed model from *GEMMA*

### Permutate GEMMA association

Script: `gemma_iterations.sh`

Rerun *GEMMA*, randomly shuffling the phenotypes each iteration to create a null distribution.

Script to shuffle the *PLINK* file: `shuffle_plink_fam.py`

## Calculate Linkage Disequilibrium

### Phase genetic variatnts

Script: `beagle_phase_vcf.sh`

Use *Beagle* to generate phased VCF from filtered genotypes.

### Calculate LD with *VCFtools*

Script: `vcftools_ld.sh`

Use *VCFtools* to calculate LD within and between chromosomes/scaffolds.

### Plot genome-wide LD

Script: `ld_plot.R`

Plot genome-wide LD and calculate LD decay using *R*.

### Plot inter-chromosomal LD matrix

Script: `plot_ld_matrix.R`

Plot a heatmap of interchromosome/scaffold LD using *R*

## Annotate genetic variants

Script: `snpEff_annotate_snps.sh`

Annotate genetic variants using the genome annotation for `BANY.1.2` and *snpEff*.

## Calculate and plot synteny

Arrange the *B. anynana* scaffolds into semi-contiguous sequences using conserved synteny against *Heliconius melpomene*

Source to *H. melpomene* genome: <http://ensembl.lepbase.org/Heliconius_melpomene_melpomene_hmel2/Info/Index>

### Create genome-wide alignments with *Mummer*

Script: `mummer_Hmel_bicyclus.sh`

Use `promer` to make alignments between the two genomes.

### Fiter alignments and generate synteny plots

Scripts: `Plot_bicyclus_heliconius_synteny_new.R`

## Generate QQplot

Script: `bany_qqplot.R`

Generate QQplot of observed vs. expected p-values using *R*

## Author

Angel G. Rivera-Colon: <angelgr2@illinois.edu>

## Notes:

Original [repo](https://bitbucket.org/angelgr2/bany_dhen_arc2019/src/default/)appears to be unavailable. Populating this one from local backups. 